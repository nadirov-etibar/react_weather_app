import axios from "axios";
import {weatherApi, weatherApiKey} from "./url";
import {getIp} from "./getIp";

export const getWeather = () => {
     return getIp().then(result => {
         return axios.get(weatherApi + result + weatherApiKey).then((response) => {
                // console.log(response.data);
                return response.data;
        }).catch((error) => {
            console.log(error)
        })
    });
}