import axios from "axios";
import {ipApi} from "./url";

export const getIp = () => {
    return axios.get(ipApi).then((response) => {
        // console.log(response.data.region_name);
        return response.data.region_name
    }).catch((error) => {
        console.log(error);
    })
}