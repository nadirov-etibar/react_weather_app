import React, {useState ,useEffect} from 'react';
import {getWeather} from "../../API/getWeather";

const WeatherCard = () => {
    const [weatherData, setWeatherData] = useState({});

    useEffect(()=>{
        getWeather().then(result => {
                setWeatherData(result);
                // console.log(result);
        })
    },[]);

    if (Object.entries(weatherData).length === 0){
        return (
            <h1>Loading...</h1>
        )
    }
    // console.log(weatherData)

    else {
        return (
            <div>
                {console.log()}
                {weatherData.weather.map((element) => (
                    <h1>{element.description}</h1>
                ))}
                {/*<h1>{weatherData.base}</h1>*/}
            </div>
        );
    }
}

export default WeatherCard;