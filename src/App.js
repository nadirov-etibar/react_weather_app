import React from 'react';
import './App.css';
import WeatherCard from "./components/weathercard/WeatherCard";

function App() {
  return (
    <div className="App">
      <WeatherCard />
    </div>
  );
}

export default App;
